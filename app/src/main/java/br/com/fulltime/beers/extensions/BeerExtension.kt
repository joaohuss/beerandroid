package br.com.fulltime.beers.extensions

import br.com.fulltime.beers.model.Beer

fun Beer.toABVValue(): String = "${this.alcoholContent.toString().replace(".", ",")}% vol."

fun Beer.toBitternessValue(): String = "${this.bitterness.toString().replace(".", ",")}"