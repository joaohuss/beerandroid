package br.com.fulltime.beers.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import br.com.fulltime.beers.R
import br.com.fulltime.beers.extensions.loadUrl
import br.com.fulltime.beers.extensions.toABVValue
import br.com.fulltime.beers.extensions.toBitternessValue
import br.com.fulltime.beers.model.Beer
import kotlinx.android.synthetic.main.activity_beer_detail.*

class BeerDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer_detail)
        setupActionBar()
        setupIntent()
    }

    private fun setupIntent() {
        val beer = intent?.getSerializableExtra("beer") as Beer
        populateView(beer)
    }

    private fun populateView(beer: Beer) {
        beerDetailImage.loadUrl(
            beer.image,
            ContextCompat.getDrawable(this, R.drawable.beer_colorized)!!
        )
        beerDetailName.text = beer.name
        beerDetailTagLine.text = beer.tagline
        beerDetailDescription.text = beer.description
        beerDetailAlcoholContent.text = beer.toABVValue()
        beerDetailBitterness.text = beer.toBitternessValue()
    }

    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.detalhes)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}