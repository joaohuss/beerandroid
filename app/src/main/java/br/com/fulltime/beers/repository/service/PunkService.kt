package br.com.fulltime.beers.repository.service

import br.com.fulltime.beers.model.Beer
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

public interface PunkService {

    companion object{
        private const val BASE_URL = "https://api.punkapi.com/"

        private val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service: PunkService = retrofit.create(PunkService::class.java)
    }

    @GET("/v2/beers")
    fun getBeers(
        @Query("page") page : String ,
        @Query("per_page") perPage : String = "80"
    ) : Call<List<Beer>>

}