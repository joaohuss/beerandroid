package br.com.fulltime.beers.ui.recyclerview.decoration

import android.graphics.Rect
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class SpaceDecoration(
    private val spanCount: Int,
    private val itemSize: Float
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildLayoutPosition(view)
        val column = position % spanCount
        val parentWidth = parent.width
        val spacing: Int = ((parentWidth - (itemSize * spanCount)) / (spanCount + 1)).toInt()
        outRect.left = spacing - column * spacing / spanCount
        outRect.right = (column + 1) * spacing / spanCount
        outRect.bottom = spacing
        if (position < spanCount) {
            outRect.top = spacing
        }
    }
}