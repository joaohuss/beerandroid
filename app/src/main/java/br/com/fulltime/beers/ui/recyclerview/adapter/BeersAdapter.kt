package br.com.fulltime.beers.ui.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import br.com.fulltime.beers.R
import br.com.fulltime.beers.extensions.loadUrl
import br.com.fulltime.beers.extensions.toABVValue
import br.com.fulltime.beers.model.Beer
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_beer.view.*

open class BeersAdapter(
    private val context: Context
) : RecyclerView.Adapter<BeersAdapter.ViewHolder>() {
    protected var onClick: ((beer: Beer) -> Unit)? = null
    private val beers: MutableList<Beer> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_list_beer, parent, false)
        return ViewHolder(view)
    }

    fun setBeersList(beers: List<Beer>) {
        this.beers.clear()
        this.beers.addAll(beers)
        notifyDataSetChanged()
    }

    fun addBeersList(beers: List<Beer>) {
        val startIndex = this.beers.size
        this.beers.addAll(beers)
        notifyItemRangeInserted(startIndex, beers.size)
    }

    fun setOnItemClick(onClick: (beer: Beer) -> Unit) {
        this.onClick = onClick
    }

    override fun getItemCount(): Int = beers.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(beers[position])
    }

    inner class ViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        private val title = itemView.itemListBeerName
        private val abv = itemView.itemListBeerAlcoholContent
        private val image = itemView.itemListBeerImage

        fun bindView(beer: Beer) {
            itemView.setOnClickListener {
                onClick?.let { it(beer) }
            }
            title.text = beer.name
            abv.text = beer.toABVValue()
            image.loadUrl(beer.image, ContextCompat.getDrawable(context, R.drawable.beer_colorized)!!)
        }
    }
}