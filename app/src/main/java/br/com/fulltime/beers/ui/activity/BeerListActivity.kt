package br.com.fulltime.beers.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import br.com.fulltime.beers.R
import br.com.fulltime.beers.extensions.setInfinityScroll
import br.com.fulltime.beers.model.Beer
import br.com.fulltime.beers.repository.BeerRepository
import br.com.fulltime.beers.ui.recyclerview.adapter.BeersAdapter
import br.com.fulltime.beers.ui.recyclerview.decoration.SpaceDecoration
import com.faltenreich.skeletonlayout.Skeleton
import com.faltenreich.skeletonlayout.applySkeleton
import kotlinx.android.synthetic.main.activity_main.*

class BeerListActivity : AppCompatActivity() {

    private val adapter by lazy { BeersAdapter(this) }
    private var page = 1
    private var isLoadingRequest = false
    private var hasMoreResult = true
    private lateinit var skeleton: Skeleton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerView()
        getBeerList()
    }

    private fun setupRecyclerView() {
        adapter.setOnItemClick { beerClicked ->
            goDetailActivity(beerClicked)
        }
        beerListRecyclerView.apply {
            this.adapter = this@BeerListActivity.adapter
            this.addItemDecoration(
                SpaceDecoration(
                    2,
                    resources.getDimension(R.dimen.recycler_view_item_width)
                )
            )
            this.setInfinityScroll(15) {
                checkMoreResult()
            }
            skeleton = this.applySkeleton(R.layout.item_list_beer, 10)
        }
        skeleton.showSkeleton()
    }

    private fun checkMoreResult() {
        if (!isLoadingRequest && hasMoreResult) {
            getBeerList()
        }
    }

    private fun getBeerList() {
        isLoadingRequest = true
        BeerRepository().getBeers(page).observe(this, Observer { beerList ->
            onResponseBeerList(beerList = beerList)
        })
    }

    private fun onResponseBeerList(beerList: List<Beer>) {
        hasMoreResult = beerList.isNotEmpty()
        if (page == 1) {
            adapter.setBeersList(beerList)
        } else {
            adapter.addBeersList(beerList)
        }
        skeleton.showOriginal()
        page++
        isLoadingRequest = false
    }

    private fun goDetailActivity(beer: Beer) {
        startActivity(Intent(this, BeerDetailActivity::class.java).apply {
            putExtra("beer", beer)
        })
    }
}