package br.com.fulltime.beers.extensions

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.setInfinityScroll(
    remainingItems: Int = 50,
    callback: () -> Unit
) {
    val listLayoutManager = this.layoutManager as LinearLayoutManager
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val totalItems = listLayoutManager.itemCount
            val pastVisibleItems = listLayoutManager.findLastVisibleItemPosition()
            if (pastVisibleItems >= 0 && pastVisibleItems > totalItems - remainingItems) {
                callback()
            }
        }
    })
}