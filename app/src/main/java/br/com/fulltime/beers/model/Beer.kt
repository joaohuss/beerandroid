package br.com.fulltime.beers.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Beer (
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("tagline") val tagline: String,
    @SerializedName("description") val description: String,
    @SerializedName("image_url") val image: String,
    @SerializedName("abv") val alcoholContent: Float,
    @SerializedName("ibu") val bitterness: Float
) : Serializable