package br.com.fulltime.beers.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.fulltime.beers.model.Beer
import br.com.fulltime.beers.repository.service.PunkService
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BeerRepository {
    fun getBeers(page: Int): LiveData<List<Beer>> {
        val liveData = MutableLiveData<List<Beer>>()
        val beers = PunkService.service.getBeers(page = page.toString())
        beers.enqueue(object : Callback<List<Beer>> {
            override fun onFailure(call: Call<List<Beer>>, t: Throwable) {
                liveData.postValue(mutableListOf())
            }

            override fun onResponse(call: Call<List<Beer>>, response: Response<List<Beer>>) {
                liveData.postValue(response.body())
            }
        })
        return liveData
    }
}