package br.com.fulltime.beers.extensions

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.loadUrl(url : String, placeholder : Drawable){
    Picasso.get().load(url).placeholder(placeholder).into(this)
}